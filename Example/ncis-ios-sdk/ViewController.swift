//
//  ViewController.swift
//  ncis-ios-sdk
//
//  Created by 1781270 on 07/15/2022.
//  Copyright (c) 2022 1781270. All rights reserved.
//

import UIKit
import ncis_ios_sdk

class ViewController: UIViewController {
    var pwaSetting = NCISPwaSetting()
    let versionUrl = "https://demo-web-content.azurewebsites.net/api/web-content/next-update"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onTouchingBtn(_ sender: Any) {
        NCISUtil.getLastVersion(url: versionUrl) { [weak self]result in
            switch result {
            case .success(let response):
                self?.downloadContentFile(url: response.downloadUrl)
            case .failure(let error):
                debugPrint("We got a failure trying to get the last Version. The error we got was: \(error.localizedDescription)")
            }
        }
    }
    
    func downloadContentFile(url:String) {
        NCISUtil.dowloadContent(contentURL: url) { result in
            switch result {
            case .success(let path):
                self.unzipFile(with: path)
            case .failure(let error):
                debugPrint("We got a failure trying to dowloadContent. The error we got was: \(error.localizedDescription)")
            }
        }
    }
    
    func unzipFile(with path:String) {
        NCISUtil.unzipFileAtDocument(from: path) { unzipResult in
            switch unzipResult {
            case .success(let folderName):
                self.showWebContentViewController(with: folderName)
            case .failure(let error):
                debugPrint("We got a failure trying to unzipFileAtDocument. The error we got was: \(error.localizedDescription)")
            }
        }
    }
    
    func showWebContentViewController(with folderPath:String) {
        pwaSetting = NCISPwaSetting(
            hasNewPackage: false,
            contentConfiguration: ContentConfiguration.unzipFolder(
                path: folderPath, port: 8080
            )
        )
        
        if pwaSetting.isCanOpenWebView {
            if let vc = NCISUtil.createViewController(
                configuration: pwaSetting.getConfiguration()
            ) {
                self.present(vc, animated: true)
            }
        }
    }
}

