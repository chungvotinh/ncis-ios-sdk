//
//  ContentConfiguration.swift
//  ncis-ios-sdk
//
//  Created by Bui Chung on 05/08/2022.
//

import Foundation

public enum ContentConfiguration: Codable {
    case unzipFolder(path:String, port: Int)
    case zipFile(path:String, port:Int)
    case zipStream(port: Int)
}
