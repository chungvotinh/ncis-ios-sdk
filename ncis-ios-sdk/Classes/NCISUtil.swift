//
//  NCISUtil.swift
//  NcisSDK
//
//  Created by Bui Chung on 14/07/2022.
//

import Foundation
import UIKit

public struct NCISUtil {
    private let versionManager: VersionManager
    
public init(versionUrl:String) {
        versionManager = VersionManager(fullURL: versionUrl)
    }
    
    public static func unzipFileAtDocument(from sourcePath:String, completion: @escaping (Result<String, Error>) -> Void) {
        let fileManager = FileManager()
        
        let sourceURL = URL(fileURLWithPath: sourcePath)
        
        let sourceFileName = sourceURL.deletingPathExtension().lastPathComponent
        
        var destinationURL = FileUtil.documentDirectory()
        destinationURL.appendPathComponent(sourceFileName)
        
        var isDir:ObjCBool = true
        if !fileManager.fileExists(atPath: destinationURL.path, isDirectory: &isDir) {
            do {
                try fileManager.createDirectory(at: destinationURL, withIntermediateDirectories: true, attributes: nil)
                try fileManager.unzipItem(at: sourceURL, to: destinationURL)
                
                completion(.success(destinationURL.path))
            } catch {
                do {
                    try fileManager.removeItem(at: destinationURL)
                    debugPrint("Extraction of ZIP archive failed with error:\(error)")
                    completion(.failure(NCISError.unzipError))
                } catch {
                    debugPrint("Extraction of ZIP archive failed with error 1:\(error)")
                    completion(.failure(NCISError.unzipError))
                }
            }
        } else {
            completion(.success(sourceFileName))
        }
    }
    
    func getLastVersion(completion: @escaping (Result<VersionResponse, Error>) -> Void) {
        versionManager.get(version: "-1", completion: completion)
    }
    
    public static func getLastVersion(url: String, completion: @escaping (Result<VersionResponse, Error>) -> Void) {
        VersionManager(fullURL: url).get(version: "-1", completion: completion)
    }

    
    public static func dowloadContent(contentURL:String, completion: @escaping (Result<String, Error>) -> Void) {
        NetworkManager.dowloadFile(fromURL: contentURL, completion: completion)
    }
    
    public static func createViewController(
        configuration: ContentConfiguration
    ) -> UIViewController? {
        return NCISViewController.newInstance(withConfig: configuration)
    }
}
