//
//  NCISPwaSetting.swift
//  NcisSDK
//
//  Created by Bui Chung on 21/08/2022.
//

import Foundation

public class NCISPwaSetting {
    public var hasNewPackage: Bool
    public var contentConfiguration: ContentConfiguration?
    
    public init(hasNewPackage: Bool = true, contentConfiguration: ContentConfiguration? = nil) {
        self.hasNewPackage = hasNewPackage
        self.contentConfiguration = contentConfiguration
    }
    
    var currentConfig: ContentConfiguration? = nil
    
    public var isCanOpenWebView: Bool {
        return validate()
    }
    
    public func validate() -> Bool {
        if hasNewPackage {
            if contentConfiguration != nil {
                currentConfig = contentConfiguration
                return true
            }
        } else {
            if let latestConfig = VersionManager.getCurrentConfiguration() {
                currentConfig = latestConfig
                return true
            }
        }
        
        return false
    }
    
    public func getConfiguration() -> ContentConfiguration {
        return currentConfig!
    }    
}

