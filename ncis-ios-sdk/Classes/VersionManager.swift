//
//  VersionManager.swift
//  NCIS PWD
//
//  Created by Bui Chung on 27/06/2022.
//

import Foundation

struct VersionManager {
    // Access Shared Defaults Object
    private let userDefaults = UserDefaults.standard
    
    private var fullURL: String
    
    public init(fullURL:String) {
        self.fullURL = fullURL
    }
    
    func getLastVersion(completion: @escaping (Result<VersionResponse, Error>) -> Void) {
        get(version: "-1", completion: completion)
    }
    
    
    func get(
        version:String,
        completion: @escaping (Result<VersionResponse, Error>) -> Void) {
            // Create the URL to fetch
            guard let url = URL(
                string: fullURL) else {
                fatalError("Invalid URL")
            }
            // Create the network manager
            let networkManager = NetworkManager()
            
            // Request data from the backend
            networkManager.request(
                fromURL: url,
                parameters: [
                    "Version": version,
                    "Environment": "development"
                ],
                completion: completion
            )
        }
    
    func needToUpdateOrNot(version:VersionResponse) -> Bool {
        let curVersion = userDefaults.integer(forKey: Constant.storageKeyForVersion)
        if let versionOnServer = Int(version.version), curVersion < versionOnServer {
            return true
        }
        
        return false
    }
    
    func updateCurrentVersion(version:VersionResponse) {
        if let versionOnServer = Int(version.version) {
            userDefaults.set(versionOnServer, forKey: Constant.storageKeyForVersion)
            userDefaults.set(version.downloadUrl, forKey: Constant.storageKeyForFileURL)
        }
    }
    
    func savePathOfContentFolder(path:String) {
        userDefaults.set(path, forKey: Constant.storageKeyForContentFolder)
    }
    
    func getPathOfContentFolder() -> String? {
        return userDefaults.string(forKey: Constant.storageKeyForContentFolder)
    }
    
    static func saveConfigurationForPWA(config: ContentConfiguration) {
        UserDefaultsManager.set(config, forKey: Constant.storageKeyForContentConfiguration)
    }
    
    static func getCurrentConfiguration() -> ContentConfiguration? {
        return UserDefaultsManager.get(forKey: Constant.storageKeyForContentConfiguration)
    }
}
