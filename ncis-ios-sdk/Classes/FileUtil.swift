//
//  FileUtil.swift
//  NCIS PWD
//
//  Created by Bui Chung on 28/06/2022.
//

import Foundation

struct FileUtil {
    static func documentDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    static func getPathFromDocumentWith(componentName: String) -> URL {
        var documentFolderURL = documentDirectory()
        documentFolderURL.appendPathComponent(componentName)
        return documentFolderURL
    }

    static func fullDocumentDirectory() -> String {
        return documentDirectory().path
    }

    
    static func append(toPath path: String,
                        withPathComponent pathComponent: String) -> String? {
        if var pathURL = URL(string: path) {
            pathURL.appendPathComponent(pathComponent)
            
            return pathURL.path
        }
        
        return nil
    }

}
