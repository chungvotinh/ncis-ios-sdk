//
//  NCISError.swift
//  ncis-ios-sdk
//
//  Created by Bui Chung on 05/08/2022.
//

import Foundation

enum NCISError: Error {
    case unknownError
    case unzipError
 }
