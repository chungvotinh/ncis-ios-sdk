//
//  Constants.swift
//  NCIS PWD
//
//  Created by Bui Chung on 26/06/2022.
//

import Foundation

struct Constant {
    static let defaultValueForEmptyString = "nil???"
    
    static let idForNotificationDemo = "demoNotificationId"
    
    static let methodCreate = "create"
    static let methodUpdate = "update"
    static let methodDelete = "delete"
    static let methodRead = "read"
    
    static let namespaceNotification = "local-notification"
    static let namespaceDeviceToken = "device-token"
    static let namespace = ""
    
    static let resultCodeOK = "OK"
    static let resultCodeFail = "FAIL"
    
    static let storageKeyForVersion = "storageKeyForVersion"
    static let storageKeyForFileURL = "storageKeyForFileURL"
    static let storageKeyForContentFolder = "storageKeyForContentFolder"
    static let storageKeyForContentConfiguration = "storageKeyForContentConfiguration"
    
    static let urlForCheckingVersion = "https://demo-web-content.azurewebsites.net/api/web-content/next-update"
    static let urlForHomePage  = "https://ncis.netlify.app"
}
