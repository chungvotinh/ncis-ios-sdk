//
//  Request.swift
//  NCIS PWD
//
//  Created by Bui Chung on 12/04/2022.
//

import Foundation

struct RequestDetail: Decodable {
    var id: String?
    var showTime:String?
    var title: String?
    var body:String?
}

struct Request: Decodable {
    var data: RequestDetail?
    var id:String
    var method:String
    var namespace: String
    var type:String
    
    var convertedDate: Date? {
        get {
            guard let rData = data?.showTime else {
                return nil
            }
            
            return TimeFormatter.date(from: rData)
        }
    }
    
    var mobileResponse: Dictionary<String, Any> {
        get {
            return [
                "id":id,
                "namespace":namespace,
                "method":method,
                "type":"mobile-response",
                "data": [
                    "success": true
                ]
            ]
        }
    }
    
    var mobileResponseStr: String? {
        get {
            Request.getStringFrom(responseDic: mobileResponse)
        }
    }
    
    func getResponseForCreating(resultCode:String, outputId:String) -> Dictionary<String, Any> {
        return [
            "id":id,
            "namespace":namespace,
            "method":method,
            "type":"mobile-response",
            "resultCode": resultCode,
            "data": [
                "id": outputId
            ]
        ]
    }
    
    func getResponseForCreatingString(resultCode:String, outputId:String) -> String? {
        return Request.getStringFrom(responseDic: getResponseForCreating(resultCode: resultCode, outputId: outputId))
    }
    
    func getResponseForUpdating(resultCode: String) -> Dictionary<String, Any?> {
        return [
            "id":id,
            "namespace":namespace,
            "method":method,
            "type":"mobile-response",
            "resultCode": resultCode,
            "data": nil
        ]
    }
    
    func getResponseForUpdatingString(resultCode: String) -> String? {
        return Request.getStringFrom(
            responseDic: getResponseForUpdating(resultCode: resultCode)
            )
    }
    
    func getResponseForDeleting(resultCode: String) -> Dictionary<String, Any?> {
        return [
            "id":id,
            "namespace":namespace,
            "method":method,
            "type":"mobile-response",
            "resultCode": resultCode,
            "data": nil
        ]
    }
    
    func getResponseForDeletingString(resultCode: String) -> String? {
        return Request.getStringFrom(
            responseDic: getResponseForDeleting(resultCode: resultCode)
            )
    }

    
    func getResponseStringForPushRegister(token: String) -> String? {
        let output = [
            "id": id,
            "namespace" : namespace,
            "method" : method,
            "type" : "mobile-response",
            "data" : [
                "success" : true,
                "deviceToken" : token
            ]
        ] as [String : Any]
        return Request.getStringFrom(responseDic: output)
    }
    
    static func getStringFrom(responseDic:Dictionary<String, Any?>) -> String? {
        do {
            let jsonData = try JSONSerialization.data(
                withJSONObject: responseDic,
                options: .prettyPrinted
            )
            // here "jsonData" is the dictionary encoded in JSON data
            return String(data: jsonData, encoding: .utf8)
        } catch {
            return nil
        }

    }
}
