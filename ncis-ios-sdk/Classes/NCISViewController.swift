//
//  ViewController.swift
//  NCIS PWD
//
//  Created by Bui Chung on 09/04/2022.
//

import UIKit
import WebKit
import ZIPFoundation
import GCDWebServer

class NCISViewController: UIViewController {
    
    static func newInstance() -> NCISViewController? {
        let myBundle = Bundle(for: NCISViewController.self)
        let storyboard = UIStoryboard(name: "Main", bundle: myBundle)
        let vc = storyboard.instantiateInitialViewController() as? NCISViewController
        return vc
    }
    
    static func newInstance(withConfig configuration:ContentConfiguration) -> UIViewController? {
        let vc = newInstance()
        vc?.configuration = configuration
        return vc
    }
    
    let notificationCenter = UNUserNotificationCenter.current()
    @IBOutlet var webView: WKWebView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    var curRequest: Request?
    var configuration:ContentConfiguration?
    var listenerName: String = "ios"
    var urlForHomePage = Constant.urlForHomePage
    let webServer = GCDWebServer()
    let uuid = UIDevice.current.identifierForVendor?.uuidString
    let versionManager = VersionManager(
        fullURL: Constant.urlForCheckingVersion
    )
    
    @IBAction func onTouchingNavButton(_ sender: AnyObject) {
        showActionsForMock()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        showLoadingIndicator()
        requestLocalNotification()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        stopWebServer()
    }
    func initDynamicWebsite(
        folderName:String,
        port:Int
    ) {
        let folderPath = FileUtil.getPathFromDocumentWith(componentName: folderName)
        
        webServer.delegate = self
        debugPrint("HTML base folder Name: \(folderName)")
        debugPrint("HTML base folder Path: \(folderPath.path)")
        webServer.addGETHandler(
            forBasePath: "/",
            directoryPath: folderPath.path,
            indexFilename: "index.html",
            cacheAge: 0,
            allowRangeRequests: true
        )
        let options: Dictionary<String, Any> = [
            GCDWebServerOption_Port:port,
            GCDWebServerOption_BonjourName:"GCD Web Server",
            GCDWebServerOption_AutomaticallySuspendInBackground: false,
            GCDWebServerOption_BindToLocalhost: true,
            GCDWebServerOption_ConnectedStateCoalescingInterval: 2.0
        ]
        try! webServer.start(options: options)
    }
    
    func stopWebServer() {
        webServer.stop()
    }
    
    func unzipFileAndLoadContent(
        zipFilePath:String,
        port:Int
    ) {
        showLoadingIndicator()
        NCISUtil.unzipFileAtDocument(from: zipFilePath) { status in
            self.hideLoadingIndicator()
            switch status {
            case .success(let folderName):
                self.initDynamicWebsite(folderName: folderName, port: port)
            case .failure(let error):
                debugPrint("We got a failure trying to get the users. The error we got was: \(error.localizedDescription)")
            }
        }
    }
    
    func loadWebView(urlStr:String) {
        if let url = URL(string: urlStr) {
            loadWebView(url: url)
        }
    }
    
    
    func loadWebView(url:URL) {
        let request: URLRequest = URLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 30)
        webView.load(request)
        webView.allowsBackForwardNavigationGestures = true
        
    }
    
    func setupWebView(urlStr:String) {
        loadWebView(urlStr: urlStr)
        setupListenerOnWebview()
    }
    
    func setupWebView(url:URL) {
        loadWebView(url: url)
        setupListenerOnWebview()
        if let rConfig = configuration {
            VersionManager.saveConfigurationForPWA(config: rConfig)
        }
    }
    
    func setupListenerOnWebview() {
        if #available(iOS 14.0, *) {
            webView.configuration.defaultWebpagePreferences.allowsContentJavaScript = true
        } else {
            // Fallback on earlier versions
        }
        webView.configuration.websiteDataStore = WKWebsiteDataStore.default()
        webView.allowsBackForwardNavigationGestures = true
        webView.configuration.userContentController.add(self, name: listenerName)
    }
    
    func requestLocalNotification() -> Void {
        notificationCenter.delegate = self
        notificationCenter.requestAuthorization(options: [.alert, .sound, .badge]){
            (permissionGranted, error) in
            self.hideLoadingIndicator()
            if (!permissionGranted) {
                debugPrint("Permission Denied")
            } else {
                self.implementBaseOnConfiguration()
            }
        }
    }
    
    func implementBaseOnConfiguration() {
        if let rConfig = self.configuration {
            switch rConfig {
            case .unzipFolder(let path, let port):
                self.initDynamicWebsite(folderName: path, port: port)
            case .zipFile(let path, let port):
                self.unzipFileAndLoadContent(zipFilePath: path, port: port)
                break
            case .zipStream(_):
                break
            }
        }
    }
    
    func doProcessAfterShowAlert() {
        if let curRequest = curRequest {
            doSendResponse(request: curRequest)
        }
    }
    
    func showLoadingIndicator() {
        indicator.hidesWhenStopped = true
        indicator.startAnimating()
    }
    
    func hideLoadingIndicator() {
        DispatchQueue.main.async {
            self.indicator.stopAnimating()
        }
    }
    
    func checkContentVersion() {
        showLoadingIndicator()
        versionManager.getLastVersion { result in
            switch result {
            case .success(let response):
                debugPrint("We got a successful result with \(response)")
                self.downloadContentFile(version: response)
            case .failure(let error):
                debugPrint("We got a failure trying to get the users. The error we got was: \(error.localizedDescription)")
                self.loadCurrentContent()
                self.hideLoadingIndicator()
            }
        }
    }
    
    func loadCurrentContent() {
        if let currFolder = self.versionManager.getPathOfContentFolder() {
            self.initDynamicWebsite(folderName: currFolder, port: 8080)
        }
    }
    
    func downloadContentFile(version:VersionResponse) {
        NetworkManager.dowloadFile(fromURL: version.downloadUrl) { result in
            switch result {
            case .success(let response):
                self.unzipFileAndLoadContent(zipFilePath: response, port: 8080)
            case .failure(let error):
                debugPrint("We got a failure trying to get the users. The error we got was: \(error.localizedDescription)")
            }
        }
    }
    
    
    // https://www.hackingwithswift.com/example-code/wkwebview/how-to-run-javascript-on-a-wkwebview-with-evaluatejavascript
    // https://stackoverflow.com/questions/24049343/call-javascript-function-from-native-code-in-wkwebview
    // https://stackoverflow.com/questions/51529662/how-to-call-this-javascript-function-from-wkwebview-swift
    // https://stackoverflow.com/questions/56692368/capture-window-postmessage-in-swift-wkwebview
    func doSendResponse(request:Request) {
        if let rStr = request.mobileResponseStr {
            doSendResponse(withString: rStr)
        }
    }
    
    func doSendResponse(withString rStr:String) {
        let jsStr = "window.postMessage(\(rStr));"
        DispatchQueue.main.async {
            self.webView.evaluateJavaScript(jsStr, completionHandler: { (result, err) in
                if (err != nil) {
                    debugPrint("doSendResponse 3 \(err.debugDescription)")
                    // show error feedback to user.
                }
            })
        }
    }
}

extension NCISViewController: GCDWebServerDelegate {
    func webServerDidConnect(_ server: GCDWebServer) {
        debugPrint("webServerDidConnect \(String(describing: webServer.bonjourServerURL)) in your web browser")
    }
    
    func webServerDidStart(_ server: GCDWebServer) {
        debugPrint("webServerDidStart \(String(describing: webServer.publicServerURL)) in your web browser")
    }
    
    func webServerDidCompleteBonjourRegistration(_ server: GCDWebServer) {
        debugPrint("webServerDidCompleteBonjourRegistration \(String(describing: server.bonjourServerURL)) \(String(describing: server.publicServerURL)) in your web browser")
        if let rULR = server.bonjourServerURL {
            setupWebView(url: rULR)
        }
    }
}


extension NCISViewController: WKScriptMessageHandler {
    func userContentController(
        _ userContentController: WKUserContentController,
        didReceive message: WKScriptMessage
    ) {
        let body = message.body as? [String : AnyObject]
        guard let rbody = body else {
            return
        }
        do {
            let jsonData = try JSONSerialization.data(
                withJSONObject: rbody,
                options: .prettyPrinted
            )
            processRequestFromPWD(jsonData: jsonData)
        } catch {
            debugPrint(error.localizedDescription)
        }
        
    }
    
    func processRequestFromPWD(jsonData:Data) {
        let jsonDecoder = JSONDecoder()
        let customer = try? jsonDecoder.decode(Request.self, from: jsonData)
        
        if let rObj = customer {
            curRequest = rObj
            if rObj.namespace == Constant.namespaceDeviceToken {
                // add token to server
                sendDeviceToken()
            } else if rObj.namespace == Constant.namespaceNotification {
                doActionsOnLocalNotification(request: rObj)
            }
            
        }
    }
    
    func sendDeviceToken() {
        if let curRequest = curRequest,
           let rUUID = uuid,
           let reqStr = curRequest.getResponseStringForPushRegister(token: rUUID) {
            doSendResponse(withString: reqStr)
        }
    }
    
    func doActionsOnLocalNotification(request:Request) {
        if request.method == Constant.methodCreate {
            createNewLocalNofication(request: request) { output in
                if let rOutput = output,
                   let rStr = request.getResponseForCreatingString(
                    resultCode: Constant.resultCodeOK, outputId: rOutput
                   ) {
                    self.doSendResponse(withString: rStr)
                }
            }
        } else if request.method == Constant.methodUpdate {
            updateNotification(request: request) { output in
                if let rStr = request.getResponseForUpdatingString(resultCode: Constant.resultCodeOK), output != nil {
                    self.doSendResponse(withString: rStr)
                }
            }
        } else if request.method == Constant.methodDelete {
            deleteNotification(request: request) { output in
                if let rStr = request.getResponseForDeletingString(resultCode: Constant.resultCodeOK), output != nil {
                    self.doSendResponse(withString: rStr)
                }
            }
        }
    }
    
    
    fileprivate func createNewLocalNofication(
        request:Request,
        _ callBack:((String?) -> (Void))? = nil
    ) {
        guard let rShowDate = request.convertedDate else {
            callBack?(nil)
            return
        }
        
        let title = request.data?.title ?? Constant.defaultValueForEmptyString
        let contentMessage = request.data?.body ?? Constant.defaultValueForEmptyString
        
        notificationCenter.getNotificationSettings {
            (settings) in
            if (settings.authorizationStatus == .authorized) {
                let outputID = UUID().uuidString
                let request = self.createRequestFrom(
                    id: outputID,
                    title: title,
                    contentMessage: contentMessage,
                    showDate: rShowDate)
                self.notificationCenter.add(request, withCompletionHandler: {
                    (error) in
                    if (error != nil) {
                        debugPrint("sendLocalNotification 1")
                        debugPrint("Error " + error.debugDescription)
                        callBack?(nil)
                    } else {
                        callBack?(outputID)
                    }
                })
            }
        }
    }
    
    func createRequestFrom(
        id: String?,
        title: String?,
        contentMessage: String?,
        showDate: Date
    ) -> UNNotificationRequest {
        let content  = UNMutableNotificationContent()
        content.title = title ?? Constant.defaultValueForEmptyString
        content.body = contentMessage ?? Constant.defaultValueForEmptyString
        content.sound = UNNotificationSound.default
        
        let dateComp = Calendar.current.dateComponents(
            [.year, .month, .day, .hour, .minute, .second],
            from: showDate
        )
        debugPrint("dateComp \(dateComp)")
        let trigger = UNCalendarNotificationTrigger(
            dateMatching: dateComp,
            repeats: false
        )
        
        let request = UNNotificationRequest(
            identifier: id ?? UUID().uuidString,
            content: content,
            trigger: trigger
        )
        return request
    }
    
    func updateNotification(
        request:Request,
        callBack:((String?) -> (Void))? = nil) {
            notificationCenter.getPendingNotificationRequests { notiRequests in
                for req in notiRequests {
                    if req.identifier == request.data?.id {
                        let updateRequest = self.createRequestFrom(
                            id: request.data?.id,
                            title: request.data?.title,
                            contentMessage: request.data?.body,
                            showDate: request.convertedDate ??  Date().addingTimeInterval(3))
                        self.notificationCenter.add(updateRequest) { error in
                            if (error != nil) {
                                debugPrint("sendLocalNotification 2")
                                debugPrint("Error " + error.debugDescription)
                                callBack?(nil)
                            } else {
                                callBack?(request.data?.id)
                            }
                        }
                        return
                    }
                }
                callBack?(nil)
            }
        }
    
    func deleteNotification(
        request: Request,
        callBack:((String?) -> (Void))? = nil
    ) {
        notificationCenter.getPendingNotificationRequests { notiRequests in
            for req in notiRequests {
                if let rDeleteID = request.data?.id, req.identifier == rDeleteID  {
                    self.notificationCenter.removePendingNotificationRequests(withIdentifiers: [rDeleteID])
                    callBack?(rDeleteID)
                    return
                }
            }
            callBack?(nil)
        }
    }
    
    
    private func showActionsForMock() -> Void {

    }
}

extension NCISViewController: UNUserNotificationCenterDelegate {
    func userNotificationCenter(
        _ center: UNUserNotificationCenter,
        willPresent notification: UNNotification
    ) async -> UNNotificationPresentationOptions {
        if #available(iOS 14.0, *) {
            return [.badge, .banner, .sound]
        } else {
            // or use some work around
            return [.badge, .sound, .alert]
        }
        
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse) async {
        debugPrint("didReceive \(response.notification)")
    }
}

// MARK: - IBActions
extension NCISViewController {
    @IBAction func cancelToMainViewController(_ segue: UIStoryboardSegue) {
    }
}
