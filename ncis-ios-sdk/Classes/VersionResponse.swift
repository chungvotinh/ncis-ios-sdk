//
//  VersionResponse.swift
//  NCIS PWD
//
//  Created by Bui Chung on 27/06/2022.
//

import Foundation

public struct VersionResponse: Decodable {
    public var version: String
    public var downloadUrl:String
}
