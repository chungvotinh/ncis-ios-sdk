//
//  TimeFormatter.swift
//  NCIS PWD
//
//  Created by Bui Chung on 27/06/2022.
//

import Foundation

struct TimeFormatter {
    static let localISOFormatter = ISO8601DateFormatter()
    
    static func date(from str:String) -> Date? {
        localISOFormatter.formatOptions.insert(.withFractionalSeconds)
        localISOFormatter.timeZone = TimeZone.current
        return localISOFormatter.date(from: str)
    }
    
    static func string(from date:Date) -> String {
        localISOFormatter.formatOptions.insert(.withFractionalSeconds)
        localISOFormatter.timeZone = TimeZone.current
        return localISOFormatter.string(from: date)
    }
}
