#
# Be sure to run `pod lib lint ncis-ios-sdk.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'ncis-ios-sdk'
  s.version          = '0.1.5'
  s.summary          = 'A native iOS for showing PWD web from NCIS'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://gitlab.com/chungvotinh/ncis-ios-sdk'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'chungvotinh' => 'chungbd90@gmail.com' }
  s.source           = { :git => 'https://gitlab.com/chungvotinh/ncis-ios-sdk', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '14.0'

  s.source_files = 'ncis-ios-sdk/**/*'
  s.dependency 'GCDWebServer', '~> 3.0'
  s.dependency 'ZIPFoundation', '~> 0.9'
  s.swift_versions = '5.5'

  # s.vendored_frameworks = 'NcisSDK.framework'
  # s.resource   = 'NcisSDK.framework/**/NcisSDK.bundle'

  # s.public_header_files = 'Pod/Classes/**/*.h'
  s.frameworks = 'UIKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
