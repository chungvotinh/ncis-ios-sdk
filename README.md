# ncis-ios-sdk

[![CI Status](https://img.shields.io/travis/1781270/ncis-ios-sdk.svg?style=flat)](https://travis-ci.org/1781270/ncis-ios-sdk)
[![Version](https://img.shields.io/cocoapods/v/ncis-ios-sdk.svg?style=flat)](https://cocoapods.org/pods/ncis-ios-sdk)
[![License](https://img.shields.io/cocoapods/l/ncis-ios-sdk.svg?style=flat)](https://cocoapods.org/pods/ncis-ios-sdk)
[![Platform](https://img.shields.io/cocoapods/p/ncis-ios-sdk.svg?style=flat)](https://cocoapods.org/pods/ncis-ios-sdk)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements
- The SDK is written in Swift(5.5 and later).
- Requires iOS 14 or later.
- It is recommended that developers use Xcode 13.0 or later.

## Installation

ncis-ios-sdk is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'ncis-ios-sdk'
```

## Usage
You just import `ncis_ios_sdk` and use `NCISUtil` to call `createViewController` for new `NCISViewController`
```swift
import ncis_ios_sdk

// Step 1: init NCISPwaSetting
var pwaSetting = NCISPwaSetting()

// after getting information about package, just assign the new object or two properties for opening web view
// 1. hasNewPackage - Bool: 
//  if true, it will take contentConfiguration to be a new configuration
//  if false, it will take the lastest contentConfiguration that show successful content to be the configuration
// 2. contentConfiguration - ContentConfiguration: the enum that list detailly below 
pwaSetting = NCISPwaSetting(
    hasNewPackage: true,
    contentConfiguration: ContentConfiguration.unzipFolder(
        path: folderPath, port: 8080
    )
)

// Step 2: show the controller contain the content of NCIS pwa web with the validation isCanOpenWebView
    if pwaSetting.isCanOpenWebView {
        if let vc = NCISUtil.createViewController(
            configuration: pwaSetting.getConfiguration()
        ) {
            self.present(vc, animated: true)
        }
    }
```

## Architecture

### ContentConfiguration
```swift
public enum ContentConfiguration {
    case unzipFolder(path:String, port: Int)
    case zipFile(path:String, port:Int)
    case zipStream(port: Int)
}
```
- `unzipFolder` for the case that you only want to give the unzip folder to SDK for showing the content
- `zipFile` for the case that you only want to give the zip file to SDK. After that SDK will unzip and show the content
- `zipStream` will update comming soon

### NCISUtil

## Author

Duc Chung, chungbd90@gmail.com

## License

ncis-ios-sdk is available under the MIT license. See the LICENSE file for more info.
